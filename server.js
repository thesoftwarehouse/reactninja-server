const WebSocket = require('ws');

const wss = new WebSocket.Server({ host:'0.0.0.0', port: 8080 });
let activeUsers = 0;

const broadcast = (server, message) => {
	server.clients.forEach((client) => {
		if (client.readyState === WebSocket.OPEN) {
			client.send(JSON.stringify(message));
		}
	});
};

wss.on('connection', (ws) => {
	activeUsers++;

	console.log('Active users:', activeUsers);

	broadcast(wss, {
		type: 'users',
		count: activeUsers
	});

	ws.on('close', () => {
		activeUsers--;

    console.log('Active users:', activeUsers);

		broadcast(wss, {
			type: 'users',
			count: activeUsers
		});
	});
});

module.exports = wss;
